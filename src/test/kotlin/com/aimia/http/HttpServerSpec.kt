package com.aimia.http

import io.micronaut.context.ApplicationContext
import io.micronaut.http.client.HttpClient
import io.micronaut.runtime.server.EmbeddedServer
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals


class HttpServerSpec: Spek({
    describe("HelloController Suite") {
        var embeddedServer : EmbeddedServer = ApplicationContext.run(EmbeddedServer::class.java)
        var client : HttpClient = HttpClient.create(embeddedServer.url)

        it("test /basic responds Hello World") {
            var rsp : String = client.toBlocking().retrieve("/basic")
            assertEquals(rsp, "Hello World")
        }

        it("test /basic/json returns json") {
            var rsp : String = client.toBlocking().retrieve("/basic/json")
            assertEquals(rsp, """{"basic":"json"}""")
        }

        afterGroup {
            client.close()
            embeddedServer.close()
        }
    }
})