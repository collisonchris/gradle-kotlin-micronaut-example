package com.aimia.app

import io.micronaut.runtime.Micronaut

object Application {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
            .packages("com.aimia")
            .mainClass(Application.javaClass)
            .start()
    }
}