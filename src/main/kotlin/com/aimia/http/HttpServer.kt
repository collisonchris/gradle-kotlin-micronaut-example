package com.aimia.http

import com.aimia.domain.Message
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.coroutines.awaitObjectResult
import com.github.kittinunf.fuel.coroutines.awaitStringResult
import com.github.kittinunf.fuel.httpGet
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

import javax.sql.DataSource

@Controller("/basic")
class HttpServer() {

    @Get("/")
    @Produces(MediaType.TEXT_PLAIN)
    fun index(): String {
        return "Hello World"
    }

    @Get("/clean")
    @Produces(MediaType.TEXT_PLAIN)
    fun clean(): String {
//        Fuel.get("https://www.bestbuy.com/api/1.0/carousel/prices?skus=6084400")
        val (request, response, result) = "https://www.bestbuy.com/api/1.0/carousel/prices?skus=6084400"
            .httpGet()
            .responseString()
        return result.get()
    }

    @Get("/json")
    @Produces(MediaType.APPLICATION_JSON)
    fun json(): Message {
        GlobalScope.launch {
            delay(1000)
            println("I launched a co-routine")
        }
        return Message("json")
    }

}

